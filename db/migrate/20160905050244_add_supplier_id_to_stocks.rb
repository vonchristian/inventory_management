class AddSupplierIdToStocks < ActiveRecord::Migration[5.0]
  def change
    add_column :stocks, :supplier_id, :integer
    add_index :stocks, :supplier_id
    add_column :stocks, :reference_number, :string
  end
end
