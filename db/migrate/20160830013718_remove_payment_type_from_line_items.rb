class RemovePaymentTypeFromLineItems < ActiveRecord::Migration[5.0]
  def change
    remove_column :line_items, :payment_type, :integer
  end
end
