class AddPurchaseDiscountToStocks < ActiveRecord::Migration[5.0]
  def change
    add_column :stocks, :purchase_discount, :decimal
    add_column :stocks, :discounted, :boolean, default: false
  end
end
