class RemoveRetailPriceFromStocks < ActiveRecord::Migration[5.0]
  def change
    remove_column :stocks, :retail_price, :decimal
    remove_column :stocks, :wholesale_price, :decimal

  end
end
