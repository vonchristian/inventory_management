class RenamePriceToRetailPrice < ActiveRecord::Migration[5.0]
  def change
    remove_column :products, :price, :decimal
    add_column :products, :retail_price, :decimal
  end
end
