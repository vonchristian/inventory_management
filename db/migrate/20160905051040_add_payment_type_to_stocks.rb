class AddPaymentTypeToStocks < ActiveRecord::Migration[5.0]
  def change
    add_column :stocks, :payment_type, :integer, default: 0
  end
end
