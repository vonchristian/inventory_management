class LineItem < ApplicationRecord
  acts_as_paranoid
  belongs_to :stock
  belongs_to :cart
  belongs_to :order
  belongs_to :employee, foreign_key: 'user_id'
  enum pricing_type: [:retail, :wholesale]
  scope :by_total_cost, -> { all.to_a.sort_by(&:total_cost) }
  # scope :created_between, lambda {|start_date, end_date| where("created_at >= ? AND created_at <= ?", start_date, end_date )}
# before_save :ensure_quantity_is_available
  validates :quantity, numericality: { less_than_or_equal_to: :stock_quantity }, on: :create
  delegate :name, to: :stock, prefix: true
  def stock_quantity
    stock.quantity
  end
  def self.cash
      all.select{|a| a.cash? }
  end
  def cash?
    order.cash?
  end
  def self.credit
      all.select{|a| a.credit? }
  end
  def credit?
    order.credit?
  end
  def self.created_between(hash={})
    if hash[:from_date] && hash[:to_date]
      from_date = hash[:from_date].kind_of?(Time) ? hash[:from_date] : Time.parse(hash[:from_date].strftime('%Y-%m-%d 12:00:00'))
      to_date = hash[:to_date].kind_of?(Time) ? hash[:to_date] : Time.parse(hash[:to_date].strftime('%Y-%m-%d 12:59:59'))
      where('created_at' => from_date..to_date)
    else
      all
    end
  end


  def total_price
    return stock.retail_price * quantity if self.retail?
    return stock.wholesale_price * quantity if self.wholesale?
  end

  def total_whole_sale_price
    stock.wholesale_price * quantity
  end
  def self.total_whole_sale_price
    self.all.to_a.sum { |item| item.unit_cost }
  end

  def self.total_price
    self.all.to_a.sum { |item| item.total_price }
  end
  private
  def ensure_quantity_is_available
    return false
  end
end
