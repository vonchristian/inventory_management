class Member < User
scope :by_total_credit, -> {with_credits.to_a.sort_by(&:total_credit).reverse }
  def to_s
    full_name
  end
  def self.with_credits
    all.select{|a| a.total_remaining_balance > 0.0 }
  end
  def self.total_payment
    all.map{|a| a.total_payment }.sum
  end
  def self.total_credit
    all.map{|a| a.total_credit }.sum
  end

  def total_credit
    Accounting::Account.find_by_name("Accounts Receivables Trade - Current").debit_entries.where(:commercial_document_id => self.id).map{|a| a.credit_amounts.pluck(:amount).sum}.sum
  end

  def total_payment
    Accounting::Account.find_by_name("Accounts Receivables Trade - Current").credit_entries.where(:commercial_document_id => self.id).map{|a| a.credit_amounts.pluck(:amount).sum}.sum
  end

  def total_remaining_balance
    total_credit - total_payment
  end
  def self.total_remaining_balance
    all.map{|a| a.total_remaining_balance }.sum
  end

  def first_credit_created_at
    if line_items.credit.present?
      line_items.first.created_at
    end
  end
  def has_credit?
    total_remaining_balance > 0
  end
end
