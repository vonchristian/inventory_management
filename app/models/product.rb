class Product < ApplicationRecord
  include PublicActivity::Common
  include PgSearch
  enum stock_status: [:available, :out_of_stock, :discontinued]
  pg_search_scope :search_by_name, :against => [:name, :bar_code]

  belongs_to :category
  has_many :stocks, dependent: :destroy
  has_many :refunds, through: :stocks
  has_many :line_items, through: :stocks
  validates :name, presence: true
  validates :retail_price, :wholesale_price, presence: true, numericality: {greater_than: 0.1}

  before_destroy :ensure_not_referenced_by_any_line_item

  def self.low_stock
    all.select{ |a| a.nearing_out_of_stock? }
  end

  def quantity
    stocks.all.sum(:quantity)
  end

  def in_stock
    stocks.sum(:quantity) - line_items.sum(:quantity)
  end

  def sold
   line_items.sum(:quantity)
  end



  def quantity_and_unit
    "#{quantity} #{unit}"
  end
  def out_of_stock?
    quantity.zero? || quantity.negative?
  end
  def nearing_out_of_stock?
    if stock_alert_count
    in_stock <= stock_alert_count
  else
    false
  end
  end
  def stock_alert
    if out_of_stock?
      "Out of Stock"
    elsif nearing_out_of_stock?
      "Low on Stock"
    end
  end
  def check_stock_status
    return self.low_stock! if quantity < stock_alert_count && quantity > 0
    return self.out_of_stock! if self.out_of_stock?
    return self.available! if !out_of_stock? || !low_stock?
  end

  private
  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'Line Items present')
      return false
    end
  end
end
