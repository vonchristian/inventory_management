class Supplier < ApplicationRecord
	has_many :stocks, dependent: :destroy
  scope :by_total_credit, -> {with_credits.to_a.sort_by(&:total_credit).reverse }
	validates :business_name, :owner, presence: true
	def self.with_credits
    all.select{|a| a.total_remaining_balance > 0.0 }
  end
	def total_credit
    Accounting::Account.find_by_name("Accounts Payable-Trade").credit_entries.where(:commercial_document_id => self.id).map{|a| a.credit_amounts.pluck(:amount).sum}.sum
  end
  def total_payment
    Accounting::Account.find_by_name("Accounts Payable-Trade").debit_entries.where(:commercial_document_id => self.id).map{|a| a.credit_amounts.pluck(:amount).sum}.sum
  end 
  def total_remaining_balance 
    total_credit - total_payment
  end
  def self.total_remaining_balance
    all.map{|a| a.total_remaining_balance }.sum
  end
  def has_credit?

  	total_remaining_balance > 0
  end
end
