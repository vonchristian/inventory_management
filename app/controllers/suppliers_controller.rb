class SuppliersController < ApplicationController

	def index
		@suppliers = Supplier.order(:business_name).all
	end

	def new
		@supplier = Supplier.new
	end

	def create
		@supplier = Supplier.create(supplier_params)
	end

	def edit
		@supplier = Supplier.find(params[:id])
	end

	def update
		@supplier = Supplier.find(params[:id])
		@supplier.update(supplier_params)
	end

	def show
		@supplier = Supplier.find(params[:id])
	end

	private

	def supplier_params
		params.require(:supplier).permit(:business_name, :owner, :mobile_number, :address)
	end
end